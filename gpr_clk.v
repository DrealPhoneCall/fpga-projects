module hello_blink ( input clk,             // 50 MHz clk
                     input duty_on,         // input pin to set clk on duty cycle
                     input duty_off,        // input pin to set clk off duty cycle
                     output reg clk_out,    // output clk out pin
                     output reg led_out );  // the clk led indicator      

    reg [31:0]  counter;

    always @(posedge clk)
        begin
            if ((counter <= duty_on) && (counter <= duty_off)
                begin
                    clk_out <= 1;
                    led_out <= 1;
                end
            else if ((counter > duty_on) && (counter <= duty_off)
                begin
                    clk_out <= 0;
                    led_out <= 0;
                end
            else
                begin
                    clk_out <= 0;
                    led_out <= 0;
                    counter <= 0;
                end
        end

endmodule