module blanking_switch (input clk,                 // 50 MHz clk
                        input tx_impulse_on,       // input pin to set clk on duty cycle
                        input tx_impulse_off,      // input pin to set clk off duty cycle
                        output reg blank_switch,   // output blank switch
                        output reg led );          // the switch led indicator      

    reg [31:0]  counter;

    always @(posedge clk)
        begin
            if ((counter <= tx_impulse_on) && (counter <= tx_impulse_on + tx_impulse_off)
                begin
                    blank_switch <= 1;
                    led_out <= 1;
                end
            else if ((counter > tx_impulse_on) && (counter <= tx_impulse_on + tx_impulse_off)
                begin
                    blank_switch <= 0;
                    led_out <= 0;
                end
            else
                begin
                    blank_switch <= 0;
                    led_out <= 0;
                    counter <= 0;
                end
        end

endmodule