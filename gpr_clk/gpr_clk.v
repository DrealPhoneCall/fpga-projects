module gpr_clk ( input clk,             // 50 MHz clk
					input milli_pin, 	  	  // whether clk is in milliseconds 
					input nano_pin,  	  	  // whether clk is in nanoseconds
					//input duty_on,       // input pin to set clk on duty cycle
					//input duty_off,      // input pin to set clk off duty cycle
					output reg clk_out,    // output clk out pin
					output reg milli_led,  // to indicate which mode is running
					output reg nano_led,   // to indicate 
					output reg led_out );  // the clk led indicator      

	reg [63:0]  counter;
	reg [31:0]	duty_on;
	reg [31:0]	duty_off;
	reg [31:0]	clk_mode_multiplier;
	
	always @(posedge clk) begin
		// set the clk mode first
		if (milli_pin) begin
			milli_led <= 1;
			nano_led <= 0;
			clk_mode_multiplier <= 1000;
			duty_on <= 5 * clk_mode_multiplier;
			duty_off <= 25 * clk_mode_multiplier;
		end
		
		if (nano_pin) begin
			milli_led <= 0;
			nano_led <= 1;
			clk_mode_multiplier <= 1000000;
			duty_on <= 5 * clk_mode_multiplier;
			duty_off <= 25 * clk_mode_multiplier;
		end
		
		// run the clock
		if ((counter <= duty_on) && (counter <= duty_off)) begin
			clk_out <= 1;
			led_out <= 1;
		end
		else if ((counter > duty_on) && (counter <= duty_off)) begin
			clk_out <= 0;
			led_out <= 0;
		end
		else begin
			clk_out <= 0;
			led_out <= 0;
			counter <= 0;
		end
	end


endmodule